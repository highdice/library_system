# Library Management System

This library management system is a web application that provides a Restful API as well as a modern, responsive, and beautiful admin tool for efficiently managing library activities. 

## Dependencies

- Git
- Composer
- Node.js or npm
- Bower

## Setup Guide

- Clone the repository by running "git clone https://highdice@bitbucket.org/highdice/library_management_system.git"
- Install dependencies by running "composer install"
- Run database migration by executing "php artisan migrate"
- Seed data by running "php artisan db:seed"